if(SEARCH_FOR_TRILINOS)
  message(STATUS "Searching for a suitable TRILINOS library ...")
  find_package(Trilinos)
endif()

if(Trilinos_FOUND)
  list(APPEND projects_found "TRILINOS")
else()
  list(APPEND projects_to_build "TRILINOS")
  externalproject_add(TRILINOS
    PREFIX TRILINOS
    URL ${TARFILE_DIR}/trilinos_9fec352.tar.gz
    CMAKE_ARGS -D CMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
                -D CMAKE_INSTALL_PREFIX:PATH=${CMAKE_INSTALL_PREFIX}
                -D BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}
                -D TPL_ENABLE_MPI=ON 
                -D TPL_ENABLE_BLAS=ON 
                -D TPL_ENABLE_LAPACK=ON 
                -D TPL_ENABLE_Boost=ON 
                  -D Boost_INCLUDE_DIRS=${BOOST_DIR}/include 
                  -D Boost_LIBRARY_DIRS=${BOOST_DIR}/lib 
                -D TPL_ENABLE_BoostLib=ON 
                  -D BoostLib_INCLUDE_DIRS=${BOOST_DIR}/include 
                  -D BoostLib_LIBRARY_DIRS=${BOOST_DIR}/lib 
                -D Trilinos_ENABLE_EXPLICIT_INSTANTIATION=ON 
                -D Trilinos_ENABLE_ALL_PACKAGES=OFF 
                -D Trilinos_ENABLE_ALL_OPTIONAL_PACKAGES=OFF 
                -D Trilinos_ENABLE_TESTS=OFF 
                -D Trilinos_ENABLE_EXAMPLES=OFF 
                -D Trilinos_ENABLE_OpenMP=OFF
                -D Trilinos_ENABLE_Teuchos=ON 
                -D Trilinos_ENABLE_Intrepid2=ON 
                -D Trilinos_ENABLE_Belos=ON 
                -D Trilinos_ENABLE_Stratimikos=ON 
                -D Trilinos_ENABLE_Thyra=ON 
                -D Trilinos_ENABLE_Tpetra=ON 
                  -D Tpetra_INST_COMPLEX_DOUBLE=OFF 
                  -D Tpetra_INST_COMPLEX_FLOAT=OFF 
                  -D Tpetra_INST_SERIAL=ON
    LOG_DOWNLOAD 1
    LOG_CONFIGURE 1
    LOG_BUILD 1
    LOG_INSTALL 1
  )
endif()
