if(SEARCH_FOR_ARBORX)
  message(STATUS "Searching for a suitable arborx library ...")
  find_package(ArborX)
endif()

if(ArborX_FOUND)
  list(APPEND projects_found "ARBORX")
else()
  list(APPEND projects_to_build "ARBORX")
  externalproject_add(ARBORX
    DEPENDS TRILINOS
    PREFIX ARBORX
    URL ${TARFILE_DIR}/arborx-4834bff.tar.gz
    CMAKE_ARGS -D CMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
               -D CMAKE_INSTALL_PREFIX:PATH=${CMAKE_INSTALL_PREFIX}
               -D BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}
               -D CMAKE_CXX_EXTENSION=OFF
               -D Kokkos_DIR=:PATH=${CMAKE_INSTALL_PREFIX}
               -D ARBORX_ENABLE_MPI=ON
    LOG_DOWNLOAD 1
    LOG_CONFIGURE 1
    LOG_BUILD 1
    LOG_INSTALL 1
  )
endif()
